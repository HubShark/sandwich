HubShark CMS
============

We are using Using HubShark's CORE and parts of various info found on the web to create our CMS. 


## What we already have
* Shiva's Versioning
* EU Cookie Law Compliance
* FOSUserBundle for User Administration
* EasyAdminBundle for Backend Administration
* i18n - German and English included
* Twiiter Bootstrap
* jQuery
* Contact Page
* Basic Site Structure implemented


## The tutorials

can be found (for the time being) in the wiki here on the project site.

## WE WILL ADD

*
*


## How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/HubShark/sandwich.git hubshark`

IMPORTANT!!! Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd hubshark`

Then fix the permissions like this:

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```

The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an Online Generated key here.

`composer install`

_answer any questions_ and it will or may end with errors message about an unkown databank. Fix that error with this:

`php bin/console doctrine:database:create`

Then Create the database schema

`php bin/console doctrine:schema:create`

And now update your database:

`php bin/console doctrine:schema:update --force`

Then create your :Admin User_ (Replace "Admin" with your adminuser name):

`php bin/console fos:user:create Admin --super-admin`

And then we run this to update everything:

`composer update --with-dependencies`


## Production environment

In order to run the sandbox in production mode at http://cmf.lo/
you need to generate the doctrine proxies and dump the assetic assets:

    php bin/console cache:warmup --env=prod --no-debug
    php bin/console assetic:dump --env=prod --no-debug
